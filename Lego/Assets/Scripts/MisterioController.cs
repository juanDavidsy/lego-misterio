using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisterioController : MonoBehaviour
{
    [Header("Componente externos")]
    public Animator controller;
    public Checkpoints checkpoint;
    Rigidbody rb;


    [Header("Velocidades y Rotacion")]
    public float currentSpeed;
    public float moveRotate;
    public float runSpeed;

    [Header("Gravedad")]
    public Vector3 movimientoY;
    public float gravity = -9.8f;
    public float jumpHeight;
    public bool is_Grounded = false;
    public Vector3 offset;
    public float sphereRadius;
    public float sphereDistance;
    public LayerMask lm;

    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        Run();
        Atack();
    }

    private void Move()
    {
        
        Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        transform.Translate((moveDirection * currentSpeed) * Time.deltaTime);
        controller.SetFloat("Speed", moveDirection.z);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * moveRotate, 0));

    }
       

    void Jump()
    {
        is_Grounded = (Physics.SphereCast(transform.position + offset, sphereRadius, Vector3.down, out RaycastHit hit,sphereDistance, lm));
     
        if (Input.GetButton("Jump")&& is_Grounded)
        {
            controller.SetTrigger("Jump");
            rb.AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
            Sounds.sound.Active_SoundsFx(transform.position, 0);
            
            
        }

    }
  
    void Run()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            currentSpeed += runSpeed;
        }

        if (Input.GetButtonUp("Fire3"))
        {
            currentSpeed -= runSpeed;
        }
        
        if(currentSpeed >= runSpeed)
        {
            controller.SetFloat("Speed", currentSpeed);
        }
    }
    void Atack()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            controller.SetTrigger("Atack");
        }
    }

    IEnumerator Waitdead()
    {
        yield return new WaitForSeconds(1f);
        controller.SetTrigger("Idle");
        transform.position = checkpoint.spawnPoint;
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Lava")
        {
            controller.SetTrigger("Die");
            StartCoroutine(Waitdead());
        }
       
        if(other.transform.tag == "Check")
        {
            checkpoint = other.GetComponent<Checkpoints>();
            checkpoint.gameObject.SetActive(false);
        }
    
    }


}
